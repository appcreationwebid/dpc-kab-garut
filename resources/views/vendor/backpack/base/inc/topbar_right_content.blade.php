<!-- This file is used to store topbar (right) items -->


{{-- <li class="nav-item d-md-down-none"><a class="nav-link" href="#"><i class="la la-bell"></i><span class="badge badge-pill badge-danger">5</span></a></li>
<li class="nav-item d-md-down-none"><a class="nav-link" href="#"><i class="la la-list"></i></a></li>
<li class="nav-item d-md-down-none"><a class="nav-link" href="#"><i class="la la-map"></i></a></li> --}}
<li class="nav-item d-md-down-none mr-2">
    <a class="nav-link" href="{{ url('admin/edit-account-info') }}">
        <i class='la la-user'></i> Pengaturan Akun
    </a>
</li>
<li class="nav-item d-md-down-none">
    <a class="nav-link" href="{{ url('admin/logout') }}">
        <i class='la la-lock'></i> Logout Akun 
    </a>
</li>
