@extends(backpack_view('blank'))
@php
    $now = \Carbon\Carbon::now();

    $breadcrumbs = [
        'DPC Kab. Garut' => backpack_url('dashboard'),
        'Selamat Datang' => false
    ];
@endphp
@php
    $widgets['before_content'][] = [
        'type'        => 'jumbotron',
        'heading'     => '<b>DPC Kab. Garut</b>',
        'content'     => '
            <h5>Selamat datang di Sistem Informasi Internal DPC PKB Kab. Garut<br>
            <hr />
            </h5>
        ',
        'button_link' => backpack_url('logout'),
        'button_text' => 'Keluar Akun',
    ];
@endphp
@section('content')

@endsection