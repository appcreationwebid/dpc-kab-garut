/*eslint-disable*/
import React from "react"

export default function Navbar(props) {
  const [navbarOpen, setNavbarOpen] = React.useState(false);
  return (
    <>
      <nav className="top-0 absolute z-50 w-full flex flex-wrap items-center justify-between px-2 py-3 navbar-expand-lg">
        <div className="container px-4 mx-auto flex flex-wrap items-center justify-between">
          <div className="w-full relative flex justify-between lg:w-auto lg:static lg:block lg:justify-start">
            <a className="text-white text-lg font-bold leading-relaxed inline-block mr-4 tracking-widest" href='/'>
              DPC Kab. Garut
            </a>
            <button className="cursor-pointer text-xl leading-none px-3 py-1 border border-solid border-transparent rounded bg-transparent block lg:hidden outline-none focus:outline-none"
              type="button"
              onClick={() => setNavbarOpen(!navbarOpen)}>
              <i className="text-white fas fa-bars"></i>
            </button>
          </div>
          <div className={"lg:flex flex-grow items-center bg-white lg:bg-opacity-0 lg:shadow-none" +(navbarOpen ? " block rounded shadow-lg" : " hidden")}
            id="example-navbar-warning">
            <ul className="flex flex-col lg:flex-row list-none mr-auto">
              <li className="flex items-center">
                <a className="lg:text-white lg:hover:text-blueGray-200 text-blueGray-700 px-3 py-4 lg:py-2 flex items-center text-xs uppercase font-bold" href="#" onClick={() => alert(`Version ${process.env.MIX_APP_VERSION}`)}>
                  <i className="lg:text-blueGray-200 text-blueGray-400 far fa-file-alt text-lg leading-lg mr-2" />{" "}
                  Version {process.env.MIX_APP_VERSION}
                </a>
              </li>
            </ul>
            <ul className="flex flex-col lg:flex-row list-none lg:ml-auto">
              <li className="flex items-center">
                {/* <CPagesDropdown /> */}
                <a href='/admin' className='lg:text-white lg:hover:text-blueGray-200 text-blueGray-700 px-3 py-4 lg:py-2 flex items-center text-xs uppercase font-bold'>
                    Login
                </a>
              </li>
              {/* <li className='flex items-center'>
                  <a href='register' className='lg:text-white lg:hover:text-blueGray-200 text-blueGray-700 px-3 py-4 lg:py-2 flex items-center text-xs uppercase font-bold'>
                      Daftar
                  </a>
              </li> */}
            </ul>
          </div>
        </div>
      </nav>
    </>
  );
}
