import React, { useState } from 'react';


const Data = () => {
    const [data, setData] = useState([
        {id:1, name:'Iqbal'},
        {id:2, name:'Rizka'},
        {id:3, name:'Elvan'}
    ])
    return (
        <div>
            <h2 className='font-bold underline font-xl'>Test Data</h2>
            {data.map((item,i) => (
                <div key={i} className='bg-white rounded-lg shadow-md py-2 px-2 mt-4 hover:transform hover:scale-105 transition-all ease-in duration-200'>
                    {item.name}
                </div>
            ))}
        </div>
    );
};


export default Data;
