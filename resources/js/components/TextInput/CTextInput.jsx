import React from 'react';
import { View, StyleSheet, TextInput } from 'react-native-web';

const CTextInput = ({
    disabled, value, onFocus, onBlur, placeholder, multiline, onChangeText,
    onKeyPress, isSecureTextEntry, numberOfLines
}) => {
    return (
        <View style={styles.container}>
            <TextInput onFocus={onFocus}
                onBlur={onBlur}
                placeholder={placeholder}
                multiline={multiline}
                numberOfLines={numberOfLines}
                secureTextEntry={isSecureTextEntry}
                style={[stylesWeb.textInputSection, {color: disabled ? '#BBBBBB' : '', backgroundColor: disabled ? 'ghostwhite' : 'white', fontFamily: 'Source Sans Pro'}]}
                value={value}
                onChangeText={onChangeText}
                onKeyPress={onKeyPress}
                disabled={disabled}
            />
        </View>
    );

}

const compare = (prevProps, nextProps) => {
    return JSON.stringify(prevProps) === JSON.stringify(nextProps)
}

export default React.memo(CTextInput, compare);

const styles = StyleSheet.create({
    container: {
        width:'100%',
        height: '100%'
    }
})

const stylesWeb = {
    textInputSection: { 
        padding:10,
        borderRadius: 5,
        borderWidth: 1,
        fontSize: 16, 
        flex: 1,
        boxShadow: `${3}px ${3}px ${3}px rgba(95,101,255,0.17)`
    }
}

