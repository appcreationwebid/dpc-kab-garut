import axios from 'axios'
import Configs from './ApiContainer.config';

const { ENDPOINT } = Configs;

//BASE-URL
const BASE_URL = Configs.BASE_URL;

//API-KEY
const API_KEY = Configs.API_KEY;

//ENDPOINT

const API = axios.create({
    headers: {
        'Access-Control-Allow-Origin' : '*',
        'Access-Control-Allow-Methods' : 'GET,PUT,POST,DELETE,PATCH,OPTIONS',
        'x-api-key': API_KEY
      },
    baseURL: BASE_URL
})

export default API