export default {
    BASE_URL: process.env.MIX_API_URL,
    API_KEY: process.env.MIX_API_KEY,
    JWT_SECRET: process.env.MIX_JWT_SECRET,
    ENDPOINT: {
        
    }
}