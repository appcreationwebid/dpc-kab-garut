import React, { Component, Fragment } from 'react';
import CNavbarAuth from '../../components/Navbars/CNavbarAuth';
import AsyncSelect from 'react-select/async';
import CFormInput from './components/CFormInput';
import CModalPopup from '../../components/Modals/CModalPopup';
import { iconWarning, iconSuccess } from '../../assets/images/icons'
import CLoading from '../../components/Loaders/CLoading';
import { getApiRegister, getApiSchoolSearch } from './action';

class Register extends Component {
    constructor() {
        super()
        this.timer
        this.state = {
            showLoading: false,
            value_select_sekolah: '',
            sekolah_options: [{ value: '', label: 'Ketikan NPSN atau Nama Sekolah' }],
            sekolah_id: '',
            nama_lengkap: '',
            jabatan: '',
            jenis_kelamin: '',
            email: '',
            password: '',
            confirm_password: '',
            agree: false,
            showPopupError: false,
            popupError: {
                icon: iconWarning,
                title: 'Title Popup',
                message: 'Message Popup',
                colorButtonPrimary: `teal`,
                buttonPrimaryText: 'Primary Button',
                buttonPrimaryAction: () => this.setState({ showPopupError: false }),
                buttonSecondaryAction: () => this.setState({ showPopupError: false }),
            }
        }
    }

    componentDidMount() {
        // this._getSearchDataSchool()
    }

    _handleTypingSearchSchool = (inputValue, callback) => {
        this.setState({ value_select_sekolah: inputValue })
        clearTimeout(this.timer)
        this.timer = setTimeout(async () => {
            if (this.state.value_select_sekolah.length > 2) {
                await this._handleGetSearchDataSchool(this.state.value_select_sekolah, callback)
            } else {
                alert('Masukan minimal 3 karakter untuk mencari sekolah')
            }
        }, 750)

    }

    _handleGetSearchDataSchool = async (inputValue, callback) => {
        try {
            const response = await getApiSchoolSearch(inputValue)
            if (response?.data?.success) {
                // console.log(response.data.output_data)
                let data_options = []
                response.data.output_data.map((item) => {
                    data_options.push({ value: item.id, label: item.nama_sekolah + ' - ' + item.npsn })
                })
                // console.log(data_options)
                this.setState({
                    sekolah_options: data_options
                }, () => callback(this._filterSchoolOptions(inputValue)))
            }
        } catch (error) {
            alert(JSON.stringify(error.message))
        }
    }

    _filterSchoolOptions = (inputValue) => {
        return this.state.sekolah_options.filter(value =>
            value.label.toLowerCase().includes(inputValue.toLowerCase())
        )
    }

    _handleChangeInput = inputType => e => {
        let dataValue = inputType == 'sekolah_id' || inputType == 'value_select_sekolah' ? e.value : e.target.value
        this.setState({
            [inputType]: dataValue
        })
    }

    _handleOnSubmit = () => {
        const { sekolah_id, nama_lengkap, jabatan, jenis_kelamin, email, password, confirm_password, agree } = this.state
        const dataForm = { sekolah_id, nama_lengkap, jabatan, jenis_kelamin, email, password, confirm_password, agree, role:'Tenaga Pendidik' }
        if (!sekolah_id || !nama_lengkap || !jabatan || !jenis_kelamin || !email || !password || !confirm_password || !agree) {
            this.setState({
                showPopupError: true,
                popupError: {
                    ...this.state.popupError,
                    icon: iconWarning,
                    title: 'Masih ada form kosong',
                    message: 'Pastikan form tidak ada yang kosong & klik kotak checkbox setuju dengan kebijakan privasi untuk melanjutkan',
                    colorButtonPrimary: 'red',
                    buttonPrimaryText: 'Periksa Kembali',
                }
            })
        }else{
            if(password != confirm_password){
                this.setState({
                    showPopupError: true,
                    popupError: {
                        ...this.state.popupError,
                        icon: iconWarning,
                        title: 'Kata Sandi Tidak Valid',
                        message: 'Pastikan kata sandi & konfirmasi kata sandi sesuai!',
                        colorButtonPrimary: 'red',
                        buttonPrimaryText: 'Periksa Kembali',
                    }
                })
            }else{
                this._handleGetApiRegister(dataForm)
            }
        }
    }

    _handleGetApiRegister = async (data) => {
        this.setState({ showLoading: true })
        try {
            const response = await getApiRegister(data)
            if(response?.data.success){
                this.setState({ 
                    showLoading: false, 
                    showPopupError: true,
                    popupError: {
                        ...this.state.popupError,
                        icon: iconSuccess,
                        title: 'Pendaftaran Berhasil',
                        message: 'Terimakasih, akun anda berhasil di daftarkan',
                        colorButtonPrimary: 'teal',
                        buttonPrimaryText: 'Kembali',
                        buttonPrimaryAction: () => window.location.replace('/'),
                        buttonSecondaryAction: () => window.location.replace('/'),
                    }
                })
            }else{
                this.setState({ 
                    showLoading: false, 
                    showPopupError: true,
                    popupError: {
                        ...this.state.popupError,
                        icon: iconWarning,
                        title: 'Inputan Tidak Valid',
                        message: response.data.output_data.map((item) => `${item} `),
                        colorButtonPrimary: 'red',
                        buttonPrimaryText: 'Periksa Kembali',
                    }
                })
            }
        }catch (error){
            this.setState({ 
                showLoading: false, 
                showPopupError: true,
                popupError: {
                    ...this.state.popupError,
                    icon: iconWarning,
                    title: 'Komunikasi Server Gagal',
                    message: 'Mohon maaf, terjadi kesalahan saat komunikasi dengan server, pastikan anda terhubung dengan internet',
                    colorButtonPrimary: 'red',
                    buttonPrimaryText: 'Periksa Kembali',
                }
            })
            console.log(error)
        }
    }

    render() {
        const { showLoading, sekolah_options, sekolah_id, nama_lengkap, jabatan, jenis_kelamin, email, password, confirm_password, agree, showPopupError, popupError } = this.state
        return (
            <Fragment>
                {showLoading && <CLoading message='Mengirim Data, Mohon Tunggu...' />}
                {showPopupError && <CModalPopup popupData={popupError} />}
                <CNavbarAuth />
                <main>
                    <section className={styles.container}>
                        <div className={styles.container2} style={{ backgroundImage: 'url(' + require('../../assets/bg_register.png').default + ')', }} />
                        <div className={styles.container3}>
                            <div className='flex content-center items-center justify-center h-full'>
                                <div className='w-full lg:w-6/12 px-2'>
                                    <div className={styles.titleCardContainer}>
                                        <div className='rounded-t mb-0 px-6 py-6'>
                                            <div className='text-center mb-3'>
                                                <h6 className='text-blueGray-500 text-sm font-bold'>
                                                    PENDAFTARAN AKUN
                                                </h6>
                                            </div>
                                            <hr className="mt-6 border-b-1 border-blueGray-300" />
                                        </div>
                                        <div className="flex-auto px-4 lg:px-10 py-10 pt-0">
                                            <div className="text-blueGray-400 text-center mb-3 font-bold">
                                                <small>Daftarkan akun dengan mengisi data yang valid</small>
                                            </div>
                                            {/* {JSON.stringify(this.state)} */}
                                            <form>
                                                <div className="relative w-full mb-3 ">
                                                    <label className={styles.labelSection} htmlFor="sekolah">
                                                        Pilih Sekolah
                                                    </label>
                                                    <AsyncSelect placeholder='Cari NPSN atau Nama Sekolah' cacheOptions
                                                        defaultOptions={sekolah_options} onChange={this._handleChangeInput('sekolah_id')}
                                                        loadOptions={this._handleTypingSearchSchool}
                                                        theme={theme => ({
                                                            ...theme,
                                                            borderRadius: 5,
                                                            colors: {
                                                                ...theme.colors,
                                                                primary: 'blue',
                                                            },
                                                        })}
                                                    />
                                                </div>

                                                <CFormInput type='text' label='Nama Lengkap' placeholder='Nama Lengkap'
                                                    value={nama_lengkap} onChange={this._handleChangeInput('nama_lengkap')}
                                                />

                                                <CFormInput type='text' label='Jabatan' placeholder='Contoh: Guru Matematika' value={jabatan} onChange={this._handleChangeInput('jabatan')} />

                                                <div className="relative w-full mb-3">
                                                    <label className={styles.labelSection} htmlFor="jenis_kelamin">
                                                        Jenis Kelamin
                                                    </label>
                                                    <select className={styles.fieldInputSection} onChange={this._handleChangeInput('jenis_kelamin')}>
                                                        <option value='' className='text-blueGray-500'>Pilih Jenis Kelamin</option>
                                                        <option value='Laki-laki'>Laki-laki</option>
                                                        <option value='Perempuan'>Perempuan</option>
                                                    </select>
                                                </div>

                                                <CFormInput type='email' label='Email' placeholder='Email' value={email} onChange={this._handleChangeInput('email')} />

                                                <CFormInput type='password' label='Password' placeholder='Password'
                                                    value={password} onChange={this._handleChangeInput('password')}
                                                />
                                                <CFormInput type='password' label='Konfirmasi Password' placeholder='Konfirmasi Password'
                                                    value={confirm_password} onChange={this._handleChangeInput('confirm_password')}
                                                />

                                                <div>
                                                    <label className="inline-flex items-center cursor-pointer">
                                                        <input id="customCheckLogin" type="checkbox"
                                                            className="form-checkbox border-0 rounded text-blueGray-700 ml-1 w-5 h-5 ease-linear transition-all duration-150"
                                                            onChange={() => this.setState({ agree: !agree })}
                                                        />
                                                        <span className="ml-2 text-sm font-semibold text-blueGray-600">
                                                            Saya setuju dengan{" "}
                                                            <a href="#pablo" className="text-lightBlue-500" onClick={(e) => e.preventDefault()}>
                                                                Kebijakan Privasi
                                                            </a>
                                                        </span>
                                                    </label>
                                                </div>

                                                <div className="text-center mt-6">
                                                    <button className={styles.buttonSubmit} type="button" onClick={this._handleOnSubmit}>
                                                        Buat Akun
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </main>
            </Fragment>
        );
    }
}

const styles = {
    container: 'relative w-full h-full py-40 min-h-screen',
    container2: 'absolute top-0 w-full h-full bg-blueGray-800 bg-no-repeat bg-full',
    container3: 'container mx-auto sm:px-4 h-full',
    titleCardContainer: 'relative flex flex-col min-w-0 break-words w-full mb-6 shadow-lg rounded-lg bg-blueGray-200 border-0',
    labelSection: 'block uppercase text-blueGray-600 text-xs font-bold mb-2',
    fieldInputSection: 'border-0 px-3 py-3 placeholder-blueGray-300 text-blueGray-600 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150',
    buttonSubmit: 'bg-blueGray-800 text-white active:bg-blueGray-600 text-sm font-bold uppercase px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 w-full ease-linear transition-all duration-150'
}

export default Register;