import React, { Fragment } from 'react';

const CSectionFuture = () => {
    return (
        <Fragment>
            <div className="flex flex-wrap">
                <div className="lg:pt-12 pt-6 w-full md:w-4/12 px-4 text-center">
                    <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-8 shadow-lg rounded-lg py-2">
                        <div className="px-4 py-5 flex-auto">
                            <a href='#'>
                                <div className="text-white p-3 text-center inline-flex items-center justify-center w-12 h-12 mb-5 shadow-lg rounded-full bg-red-600">
                                    <i className="fas fa-home"></i>
                                </div>
                                <h6 className="text-xl font-semibold">Objektif</h6>
                                <p className="mt-2 mb-4 text-blueGray-500">
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                                </p>
                            </a>
                        </div>
                    </div>
                </div>

                <div className="w-full md:w-4/12 px-4 text-center">
                    <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-8 shadow-lg rounded-lg  py-5">
                        <div className="px-4 py-5 flex-auto">
                            <a href='#'>
                                <div className="text-white p-3 text-center inline-flex items-center justify-center w-12 h-12 mb-5 shadow-lg rounded-full bg-sky-400">
                                    <i className="fas fa-home"></i>
                                </div>
                                <h6 className="text-xl font-semibold">Sistematis</h6>
                                <p className="mt-2 mb-4 text-blueGray-500">
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                                </p>
                            </a>
                        </div>
                    </div>
                </div>

                <div className="pt-6 w-full md:w-4/12 px-4 text-center">
                    <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-8 shadow-lg rounded-lg py-5">
                        <div className="px-4 py-5 flex-auto">
                            <a href='#'>
                                <div className="text-white p-3 text-center inline-flex items-center justify-center w-12 h-12 mb-5 shadow-lg rounded-full bg-black">
                                    <i className="fas fa-home"></i>
                                </div>
                                <h6 className="text-xl font-semibold">Akuntabel</h6>
                                <p className="mt-2 mb-4 text-blueGray-500">
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                                </p>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </Fragment>
    );
}

export default CSectionFuture;
