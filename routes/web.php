<?php

use App\Http\Controllers\ClearAppController;
use App\Http\Controllers\Site\HomepageController;
use App\Http\Controllers\Site\RegisterController;
use App\Http\Controllers\TesterController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('homepage');
// });

Route::get('/', [HomepageController::class, 'index'])->name('homepage');
Route::get('/register', [RegisterController::class, 'index'])->name('register');

Route::group(['middleware' => ['web', 'admin'], 'prefix' => config('backpack.base.route_prefix', 'admin')], function () {
    Route::get('test1', [TesterController::class, 'index']);
    Route::get('app-clear', ClearAppController::class);
});
