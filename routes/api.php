<?php

use App\Http\Controllers\API\Auth\RegisterController;
use App\Http\Controllers\API\MasterData\MasterDataController;
use App\Http\Controllers\API\WilayahService;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['prefix' => 'auth'],function(){
    Route::post('register', RegisterController::class);
});

Route::group(['prefix' => 'wilayah'], function(){
    Route::get('get-kabupaten', [WilayahService::class, 'getKabupaten'])->name('getKabupatenAPI');
    Route::get('get-kecamatan', [WilayahService::class, 'getKecamatan']);
    Route::get('get-kelurahan', [WilayahService::class, 'getKelurahan']);
});

Route::group(['prefix' => 'data-master'], function(){
    Route::get('category', [MasterDataController::class, 'category']);
});

