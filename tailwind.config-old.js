module.exports = {
  purge: [
    './resources/**/*.blade.php',
    './resources/**/*.js',
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    screens: {
      'tablet': '640px',
      // => @media (min-width: 640px) { ... }

      'laptop': '1024px',
      // => @media (min-width: 1024px) { ... }

      'desktop': '1280px',
      // => @media (min-width: 1280px) { ... }
    },
    extend: {
      opacity: {
        '15': '0.15',
        '35': '0.35',
        '50': '0.50',
        '75': '0.75',
        '100': '1'
      }
    },
  },
  variants: {
    extend: {
      backgroundColor: ['active'],
      opacity: ['active'],
      transform: ['hover', 'focus'],
    },
  },
  plugins: [],
}
