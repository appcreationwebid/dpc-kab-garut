<?php

namespace App\Http\Controllers\API\MasterData;

use App\Helpers\GlobalHelper;
use App\Http\Controllers\Controller;
use App\Models\Kategori;
use Illuminate\Http\Request;

class MasterDataController extends Controller
{
    public function index(){
        return 'Master Data Service';
    }

    public function category(){
        $category = Kategori::limit(20)->inRandomOrder()->get();
        $category = $category->map(function($item, $key){
            return collect($item)->except(['created_at', 'updated_at']);
        });
        if($category){
            return GlobalHelper::createResponse(true, 'Category has successfuly retrieve', $category);
        }else{
            return GlobalHelper::createResponse(false, 'Failed get data category');
        }
    }
}
