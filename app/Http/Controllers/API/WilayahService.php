<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Wilayah\Kabupaten;
use App\Models\Wilayah\Kecamatan;
use App\Models\Wilayah\Kelurahan;
use Illuminate\Http\Request;

class WilayahService extends Controller
{
    public function index(){
        return 'hello';
    }

    public function getKabupaten(Request $request){
        $form = collect($request->input('form'))->pluck('value', 'name');

        $options = Kabupaten::query();

        if(!$form['provinsi_id']){
            return [];
        }
        if($form['provinsi_id']){
            $options = $options->where('provinsi_id', $form['provinsi_id']);
        }

        $this->searchTerm($request, $options);
        
        return $options->paginate(10);
    }

    public function getKecamatan(Request $request){
        $form = collect($request->input('form'))->pluck('value', 'name');
        $options = Kecamatan::query();
        if(!$form['kabupaten_id']){
            return [];
        }
        if($form['kabupaten_id']){
            $options = $options->where('kabupaten_id', $form['kabupaten_id']);
        }
        $this->searchTerm($request, $options);

        return $options->paginate(10);
    }

    public function getKelurahan(Request $request){
        $form = collect($request->input('form'))->pluck('value', 'name');
        $options = Kelurahan::query();
        if(!$form['kecamatan_id']){
            return [];
        }
        if($form['kecamatan_id']){
            $options = $options->where('kecamatan_id', $form['kecamatan_id']);
        }
        $this->searchTerm($request, $options);
        
        return $options->paginate(10);
    }

    private function searchTerm(Request $request, $options){
        $search_term = $request->input('q');
        if ($search_term) {
            return  $options->where('name', 'LIKE', '%'.$search_term.'%');
        }else{
            return false;
        }
    }
}
