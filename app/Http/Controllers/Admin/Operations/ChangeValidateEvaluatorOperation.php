<?php

namespace App\Http\Controllers\Admin\Operations;

use Illuminate\Support\Facades\Route;

trait ChangeValidateEvaluatorOperation
{
    /**
     * Define which routes are needed for this operation.
     *
     * @param string $segment    Name of the current entity (singular). Used as first URL segment.
     * @param string $routeName  Prefix of the route name.
     * @param string $controller Name of the current CrudController.
     */
    protected function setupChangeValidateEvaluatorRoutes($segment, $routeName, $controller)
    {
        Route::get($segment.'/{id}/changevalidateevaluator', [
            'as'        => $routeName.'.changevalidateevaluator',
            'uses'      => $controller.'@changevalidateevaluator',
            'operation' => 'changevalidateevaluator',
        ]);
    }

    /**
     * Add the default settings, buttons, etc that this operation needs.
     */
    protected function setupChangeValidateEvaluatorDefaults()
    {
        $this->crud->allowAccess('changevalidateevaluator');

        $this->crud->operation('changevalidateevaluator', function () {
            $this->crud->loadDefaultOperationSettingsFromConfig();
        });

        $this->crud->operation('list', function () {
            // $this->crud->addButton('top', 'changevalidateevaluator', 'view', 'crud::buttons.changevalidateevaluator');
            // $this->crud->addButton('line', 'changevalidateevaluator', 'view', 'crud::buttons.changevalidateevaluator');
            $this->crud->addButtonFromView('line', 'changevalidateevaluator', 'changevalidateevaluator', 'beginning');
        });
    }

    /**
     * Show the view for performing the operation.
     *
     * @return Response
     */
    public function changevalidateevaluator($id)
    {
        $changeValidateEvaluator = $this->crud->model->find($id);
        $changeValidateEvaluator->validasi_penilai =  $changeValidateEvaluator->validasi_penilai=='Pending'?'Divalidasi':'Pending';
        $changeValidateEvaluator->save();

        return (string) $changeValidateEvaluator->push();
    }
}
