<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\PengurusRequest;
use App\Models\Wilayah\Kabupaten;
use App\Models\Wilayah\Kecamatan;
use App\Models\Wilayah\Kelurahan;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class PengurusCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class PengurusCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Pengurus::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/pengurus');
        CRUD::setEntityNameStrings('Pengurus', 'Pengurus');
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::setFromDb(); // columns

        $this->crud->column('provinsi_id')->type('relationship')->attribute('name');
        $this->crud->column('kabupaten_id')->type('relationship')->attribute('name');
        $this->crud->column('kecamatan_id')->type('relationship')->attribute('name');
        $this->crud->column('kelurahan_id')->type('relationship')->attribute('name');

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']); 
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(PengurusRequest::class);

        CRUD::setFromDb(); // fields

        $this->crud->modifyField('jabatan_id', [
            'type' => 'select2',
            'name' => 'jabatan_id',
            'entity' => 'jabatan', // the relationship name in your Model
            'attribute' => 'nama', // attribute on Article that is shown to admin,
        ]);

        $this->crud->modifyField('jenis_pengurus_id', [
            'type' => 'select2',
            'name' => 'jenis_pengurus_id',
            'entity' => 'jenis_pengurus', // the relationship name in your Model
            'attribute' => 'nama', // attribute on Article that is shown to admin,
        ]);

        $this->crud->modifyField('jenis_kelamin', [
            'label'       => "Jenis Kelamin",
            'type'        => 'select_from_array',
            'options'     => ['' => 'Pilih Jenis Kelamin', 'Laki-laki' => 'Laki-laki', 'Perempuan' => 'Perempuan'],
            'allows_null' => false,
            'default'     => '',
        ]);

        $this->crud->modifyField('provinsi_id', [
            'type' => 'relationship',
            'name' => 'provinsi_id',
            'entity' => 'provinsi', // the relationship name in your Model
            'attribute' => 'name', // attribute on Article that is shown to admin,
        ]);

        $this->crud->modifyField('kabupaten_id', [
            'label' => 'Kabupaten',
            'name' => 'kabupaten_id',
            'type' => 'select2_from_ajax',
            'entity' => 'kabupaten',
            'model' => Kabupaten::class,
            'attribute' => 'name',
            'placeholder' => 'Pilih Kabupaten',
            'minimum_input_length' => 0,
            'dependencies' => ['provinsi_id'],
            'data_source' => route('getKabupatenAPI'),
            'method' => 'GET',
            'include_all_form_fields' => true
        ]);

        $this->crud->modifyField('kecamatan_id', [
            'label' => 'Kecamatan',
            'name' => 'kecamatan_id',
            'type' => 'select2_from_ajax',
            'entity' => 'kecamatan',
            'model' => Kecamatan::class,
            'attribute' => 'name',
            'placeholder' => 'Pilih Kecamatan',
            'minimum_input_length' => 0,
            'dependencies' => ['kabupaten_id'],
            'data_source' => url('api/wilayah/get-kecamatan'),
            'method' => 'GET',
            'include_all_form_fields' => true
        ]);

        $this->crud->modifyField('kelurahan_id', [
            'label' => 'Kelurahan',
            'name' => 'kelurahan_id',
            'type' => 'select2_from_ajax',
            'entity' => 'kelurahan',
            'model' => Kelurahan::class,
            'attribute' => 'name',
            'placeholder' => 'Pilih Kelurahan',
            'minimum_input_length' => 0,
            'dependencies' => ['kecamatan_id'],
            'data_source' => url('api/wilayah/get-kelurahan'),
            'method' => 'GET',
            'include_all_form_fields' => true
        ]);
        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number'])); 
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
