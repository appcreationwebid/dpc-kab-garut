<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HomepageController extends Controller
{
    public function index(){
        $title = '';
        
        $compact = compact('title');

        return view('homepage', $compact);
    }
}
