<?php

namespace App\Helpers;

class GlobalHelper
{
    public static function createResponse($success, $message, $data = null, $additional_data = null)
    {
        $api_key = env('MIX_API_KEY');
        $response = [
            'success' => $success,
            'message' => $message,
        ];
        if ($data) {
            $response += [
                'output_data' => $data
            ];
        }
        if ($additional_data) {
            $response += [
                'additional_data' => $additional_data
            ];
        }
        if (request()->header('x-api-key') == $api_key) {
            return response()->json($response);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Api Key not valid',
            ], 403);
        }
    }

    public static function noHp($noHp){
        return "0" . substr($noHp, 1, 3) . "-" . substr($noHp, 4, 4) . "-" . substr($noHp, 8, 4);
    }
}
